package az.ingress.kafkaconsumer.model;

import lombok.*;
import lombok.experimental.FieldDefaults;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class Product {
     String productId;
     String title;
     BigDecimal price;
     Integer quantity;
}
