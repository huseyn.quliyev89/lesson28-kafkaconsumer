package az.ingress.kafkaconsumer.service;

import az.ingress.kafkaconsumer.model.Product;
import lombok.extern.slf4j.Slf4j;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.RetryableTopic;
import org.springframework.retry.annotation.Backoff;
import org.springframework.stereotype.Service;

import java.net.SocketTimeoutException;

@Service
@Slf4j
public class ProductServiceImpl implements ProductService {

//    @Override
////Blocking Retry
//    @KafkaListener(topics = "product-created-events-topic", groupId = "product-created-events", concurrency = "3")
//    public void consume(ConsumerRecord<String, Product> record) {
//        log.info("Message received: " + record.value());
//        throw new NullPointerException("SocketTimeoutException");
//    }

    @Override
//Non-Blocking Retry
    @RetryableTopic(
            backoff = @Backoff(value = 3000L),
            attempts = "2",
            include = SocketTimeoutException.class)
    @KafkaListener(topics = "product-created-events-topic", groupId = "product-created-events", concurrency = "3")
    public void consume(ConsumerRecord<String, Product> record) throws SocketTimeoutException {
        log.info("Message received: " + record.value());
        if (record.topic().equals("product-created-events-topic"))
        {
            throw new SocketTimeoutException("SocketTimeoutException");

        }
    }
}
