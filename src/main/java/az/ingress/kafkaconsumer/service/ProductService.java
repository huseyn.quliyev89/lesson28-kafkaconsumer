package az.ingress.kafkaconsumer.service;

import az.ingress.kafkaconsumer.model.Product;
import org.apache.kafka.clients.consumer.ConsumerRecord;

import java.net.SocketTimeoutException;

public interface ProductService {
    void consume(ConsumerRecord<String, Product> record) throws SocketTimeoutException;
}
